<!DOCTYPE html>
<html lang="en">
<head>
  <title>Restaurant Management System</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <link href="css/style.css" rel="stylesheet">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<style>
input[type=text], select {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}
</style>
<body style="background-color: white">
<nav class="navbar navbar-inverse">
  <div class="container">
      <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Peacock Restaurant</a>
    </div>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="index.php">Home</a></li>
      <li><a href="aboutus.php">Aboutus</a></li>
      <li><a href="register.php">Registration</a></li>
      <li><a href="feedback.php">Feedback</a></li>
      <li><a href="contact.php">Contactus</a></li>
       <li><a href="login.php">Login</a></li>
    </ul>
  </div>
</nav>
    
    <div id="content">
            <div class="container-fluid decor_bg" id="login-panel">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="panel panel-primary" >
                            <div class="panel-heading"  >
                                <h4>Login</h4>
                            </div>
                            <div class="panel-body">
                      
                                <form action="login-submit.php" method="POST">
                                    
                                    <div class="form-group">
                                        <input type="text" class="form-control"  placeholder="email" name="email" required>
                                    </div>
                                    <div class="form-group">
                                  
                                        <input type="password" class="form-control"  placeholder="password" name="password" required>
                                    </div>
                                 <label for="logintype">Logintype</label>
    <select id="logintype" name="logintype" required>
        <option> </option>
      <option>Admin</option>
      <option>User</option>
      <option>Employee</option>
    </select>
                                   
 
          
                                  
                                       
                                     
                                    
                                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                    <?php echo filter_input(INPUT_GET,'error'); ?>
                                    <button type="reset" name="clear" class="btn btn-primary">Clear</button><br><br>
                                </form><br/>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
    

<div class="navbar navbar-inverse navbar-fixed-bottom">
</div>
</body></html>