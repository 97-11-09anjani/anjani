<!DOCTYPE html>
<html lang="en">
<head>
  <title>Restaurant Management System</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <link href="css/style.css" rel="stylesheet">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body style="background-color: black">
<nav class="navbar navbar-inverse">
  <div class="container">
      <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Peacock Restaurant</a>
    </div>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="admin.php">Home</a></li>
      <li><a href="staff.php">Staffpanel</a></li>
      <li><a href="menu.php">Menupanel</a></li>
      <li><a href="news.php">Newspanel</a></li>
       <li><a href="logout.php">Logout</a></li>
    </ul>
  </div>
</nav>
    <div id="content">
            <div class="container-fluid decor_bg" id="login-panel">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="panel panel-default" >
                            <div class="panel-heading" style="background-color:darkseagreen ">
                                <h4>ADD EMPLOYEE DETAILS</h4>
                            </div>
                            <div class="panel-body" style="background-color:darkseagreen ">
                      
                                <form action="add-submit.php" method="POST">
                                    
                                    <div class="form-group">
                                        <input type="text" class="form-control"  placeholder="Name" name="name" required>
                                    </div>
                                    <div class="form-group">
                                  
                                        <input type="email" class="form-control"  placeholder="email" name="email" required>
                                    </div>
                                    <div class="form-group">
                                    <input type="radio" name="gender" value="male" checked> Male
  <input type="radio" name="gender" value="female"> Female
  <input type="radio" name="gender" value="other"> Other
                                    </div>
                                    <div class="form-group"> 
                                     <input type="text" class="form-control"  placeholder="Contact" name="contact" required>
                                    <?php echo filter_input(INPUT_GET,'m2');; ?>
                                    </div>
                                    <div class="form-group">
                          <textarea class="form-control" name="address" required placeholder="Address" rows="3"></textarea>
                                    </div>
                                    
                                    
                                    
                                    <button type="submit" name="submit" class="btn btn-default">Submit</button>
                                    <?php echo filter_input(INPUT_GET,'error'); ?>
                                    <button type="reset" name="clear" class="btn btn-default">Clear</button><br><br>
                                </form><br/>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="navbar navbar-inverse navbar-fixed-bottom">
</div>

</body>
</html>