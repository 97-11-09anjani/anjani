<html lang="en">
    <head>
        <title>Restaurant Management System</title>
        <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <link href="css/style.css" rel="stylesheet">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    </head>
    <body>
       <div class="navbar navbar-inverse navbar-fixed-top"style="background-color: darkslategray">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
            </button>
            <a class="navbar-brand" href="#">Peacock Restaurant</a>
    </div>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="index.php">Home</a></li>
      <li><a href="aboutus.php">Aboutus</a></li>
      <li><a href="register.php">Registration</a></li>
      <li><a href="feedback.php">Feedback</a></li>
      <li><a href="contact.php">Contactus</a></li>
       <li><a href="login.php">Login</a></li>
    </ul>
  </div>
     <div id="content">
            <div class="container-fluid decor_bg" id="login-panel">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="panel panel-default" style="background-color: darkkhaki">
                            <div class="panel-heading" style="background-color: darkkhaki">
                                <h4>REGISTRATION FORM</h4>
                            </div>
                            <div class="panel-body" style="background-color: darkkhaki">
                      
                                <form action="register-submit.php" method="POST">
                                    <div class="form-group">
                                        <input type="text" class="form-control"  placeholder="Username" name="username" required>
                                    </div>
                                    <div class="form-group">
                                <input type="password" class="form-control" placeholder="Password" pattern=".{6,}" name="password" required>
                            </div>
                                    
                                    <div class="form-group">
                                        <input type="text" class="form-control"  placeholder="Name" name="name" required>
                                    </div>
                                    <div class="form-group">
                                      
                                  
                                        <input type="email" class="form-control"  placeholder="email" name="email" required>
                                    </div>
                                    <div class="form-group">
                                  
                                        <input type="text" class="form-control"  placeholder="Contact" maxlength="10" size="10" name="contact" required>
                                    <?php echo filter_input(INPUT_GET,'m2');; ?>
                            
                                    </div>
                                    <div class="form-group">
                                    <input type="radio" name="gender" value="male" checked> Male
  <input type="radio" name="gender" value="female"> Female
  <input type="radio" name="gender" value="other"> Other
                                    </div>
                                    <div class="form-group">
                          <textarea class="form-control" name="address" required placeholder="Address" rows="3"></textarea>
                                    </div>
                                    <label for="logintype">Logintype</label>
    <select id="logintype" name="logintype">
        <option> </option>
      <option>Admin</option>
      <option>User</option>
      <option>Employee</option>
    </select>
                                   
                                    <div class="checkbox">
    <label><input type="checkbox" checked> Accept the terms and conditions of our service</label>
  </div>
                                    <button type="submit" name="submit" class="btn btn-default">Submit</button>
                                    <?php echo filter_input(INPUT_GET,'error'); ?>
                                    <button type="reset" name="clear" class="btn btn-default">Clear</button><br><br>
                                    
                                </form><br/>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        

</body>
</html>