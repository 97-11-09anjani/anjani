<!DOCTYPE html>
<html lang="en">
<head>
  <title>Restaurant Management System</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <link href="css/style.css" rel="stylesheet">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body style="background-color: black">
<nav class="navbar navbar-inverse">
  <div class="container">
      <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Admin Panel</a>
    </div>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="admin.php">Home</a></li>
      <li><a href="staff.php">Staffpanel</a></li>
      <li><a href="menu.php">Menupanel</a></li>
      <li><a href="news.php">Newspanel</a></li>
       <li><a href="logout.php">Logout</a></li>
    </ul>
  </div>
</nav>
    <div id="content">
            <div class="container-fluid decor_bg" id="login-panel">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="panel panel-default" >
                            <div class="panel-heading" style="background-color:darkseagreen ">
                                <h4>ADD MENU DETAILS</h4>
                            </div>
                            <div class="panel-body" style="background-color:darkseagreen ">
                      
                                <form action="addmenu-submit.php" method="POST">
                                    
                                    <div class="form-group">
                                        <input type="text" class="form-control"  placeholder="Productname" name="productname" required>
                                    </div>
                                    <div class="form-group">
                                  
                                        <input type="text" class="form-control"  placeholder="Category" name="category" required>
                                    </div>
                       
                                    <div class="form-group"> 
                                     <input type="text" class="form-control"  placeholder="Price" name="price" required>
                               
                                    </div>
                                    <div class="file-field">
    <div class="input-group">
  <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
  </div>
  <div class="custom-file">
    <input type="file" name="image" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
    <label class="custom-file-label" for="inputGroupFile01"></label>
  </div>
</div>
                                    
                                    
                                    
                                    <button type="submit" name="submit" class="btn btn-default">Submit</button>
                                    <?php echo filter_input(INPUT_GET,'error'); ?>
                                    <button type="reset" name="clear" class="btn btn-default">Clear</button><br><br>
                                </form><br/>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="navbar navbar-inverse navbar-fixed-bottom">
</div>

</body>
</html>

