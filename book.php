<html lang="en">
<head>
  <title>Restaurant Management System</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <link href="css/style.css" rel="stylesheet">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body style="background-color: peru">
<nav class="navbar navbar-inverse">
  <div class="container">
      <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">User Portal</a>
    </div>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="user.php">Home</a></li>
      <li><a href="menu2.php">Menu</a></li>
      <li><a href="book.php">Online Booking</a></li>
      <li><a href="status.php">Check Status</a></li>
       <li><a href="logout.php">Logout</a></li>
    </ul>
  </div>
</nav>
<div id="content">
            <div class="container-fluid decor_bg" id="login-panel">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="panel panel-default" >
                            <div class="panel-heading">
                                <h4>Online Table Booking</h4>
                            </div>
                            <div class="panel-body">
                      
<form action="book-submit.php" method="POST">
                                    <div class="form-group">
                                        <label for="tableno">Table Number</label>
    <select id="tableno" name="tableno">
        <option> </option>
      <option>Tableno.1</option>
      <option>Tableno.2</option>
      <option>Tableno.3</option>
      <option>Tableno.4</option>
      <option>Tableno.5</option>
      <option>Tableno.6</option>
      <option>Tableno.7</option>
      <option>Tableno.8</option>
      <option>Tableno.9</option>
      <option>Tableno.10</option>
      
      
    </select>
                                        
                                    </div>
    <div class="form-group">
        <label for="datetime">Datetime</label>
        <input type="datetime-local"  name="datetime">
    </div>
    
                                    <div class="form-group">
                                        <label for="seats">Number of Seats</label>
                                        <input type="text" name="seats">
                                        
                                    </div>
                             
                                    
                                    <button type="submit" name="submit" class="btn btn-default">Submit</button>
                             <?php echo filter_input(INPUT_GET,'error'); ?>
                                    <button type="reset" name="clear" class="btn btn-default">Clear</button><br><br>
                                </form><br/>
</body></html>







