Restaurant Management system

According to the problem given,I found a solution of designing a web application.

My application consists of three major modules which furthur has submodules in it. Theh are basically:

1.Admin

2. Employee(staff)

3.User

  Admin module contains the submodules such as : 

1. Staff Panel

2. Menu Panel
 
3. News Panel

	Employee Module contains the submodules such as:

1. Add Order

2. Generate Bill

3. Verify Booking


        User Module contains the submodules such as:

1. Menu

2. Online Booking

3. Checking status


       Along with the above modules my application has the Feedback module.

 The application has the Technical requirements as:

1. Xampp server (local server)

2. Php,html,css,bootstrap are the basic requirements in my application.



   The basic idea of this application is:

 Every person/user who wants to go through this application should be registered first.
 Based on the details that the user has given they can login by clicking the login , 
 then based on whether the login details are the same as the details the were given by the user
 it redirects to the other page based on the type of login.

 Suppose that the Admin has logged in with his credentials, then admin.php page gets opened and admin can perform the operations that he want.
 Such as :
	Admin can view the users who are registered.
	Admin can view the feedback given by the users with the applicaton.
        Admin can add an employee and view the employess.
	Admin can add menus and view the menu.
	Admin can add the News and delete the news.
  

Suppose that the Employee has logged in with the credentials, the employee.php page opens and employee can perform the operations such as:

	Employee can add order that is given by the user.
	Employee can Generate Bill for the given order.
	Employee can verify or allow the online table booking for the user.

Suppose that the User has logged in with the credentials, the user.php pages opens and user can perform the operations such as:

	User can view the menu items available in the restaurant.
	User can book a table in the Restaurant through online and check the status of the booking.
  
Execution process:


1. Run the Xampp server with mysql and Apache servers.

2. I Used Netbeans  IDE to write my code and I have written the code in php.

3. Run the code in IDE.

4. Automatically the Browser opens the application.
